package com.solvento.ss.support;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

public interface HqlDaoSupport {
	/**
	 * <p>批次處理(update or delete 多筆時可用)</p>
	 * <p>註：此功能有獨立的交易管理，不受其他管理設定干擾</p>
	 * @param hql
	 */
	void batchExecuteBySql(String hql);
	
	@SuppressWarnings("rawtypes")
	int countAll(Class targetClass);
	
	/**
	 * 取得HQL得到的總筆數(傳入的HQL請勿帶有select sum(1)字眼，用from XXX開頭就好)
	 * @param hql
	 * @param paramMap
	 * @return
	 */
	int countByHql(String hql, Map<String, Object> paramMap);

	/**
	 * 取得HQL得到的總筆數(傳入的HQL請勿帶有select sum(1)字眼，用from XXX開頭就好)
	 * @param hql
	 * @param params
	 * @return
	 */
	int countByHql(String hql, Object[] params);
	
	/**
	 * 根據HQL取得Entity陣列
	 * @param hql
	 * @param paramMap
	 * @return
	 */
	List<Object> findByHql(String hql, Map<String, Object> paramMap);

	<T> List<T> findByHql(String hql, Map<String, Object> paramMap, Class<T> targetClass);
	
	/**
	 * 根據HQL取得Entity陣列
	 * @param hql
	 * @param paramMap
	 * @param offset
	 * @param limit
	 * @return
	 */
	List<Object> findByHql(String hql, Map<String, Object> paramMap, int offset, int limit);

	<T> List<T> findByHql(String hql, Map<String, Object> paramMap, int offset, int limit, Class<T> targetClass);
	
	/**
	 * 根據HQL取得Entity陣列
	 * @param hql
	 * @param params
	 * @return
	 */
	List<Object> findByHql(String hql, Object[] params);

	<T> List<T> findByHql(String hql, Object[] params, Class<T> targetClass);

	/**
	 * 根據HQL取得Entity陣列
	 * @param hql
	 * @param params
	 * @param offset
	 * @param limit
	 * @return
	 */
	List<Object> findByHql(String hql, Object[] params, int offset, int limit);

	<T> List<T> findByHql(String hql, Object[] params, int offset, int limit, Class<T> targetClass);
	
	List<Object> findByNativeSql(String sql, int offset, int limit);
	<T> List<T> findByNativeSql(String sql, int offset, int limit, Class<T> targetClass);

	/**
	 * 取得符合特定欄位值的Entity列表
	 * @param propertyName
	 * @param value
	 * @return
	 */
	<T> List<T> findByProperty(String propertyName, Object value, Class<T> targetClass);
	
	List<Object> findByStoredProcedure(String hql, Map<String, Object> paramMap);
	
	<T> List<T> findByStoredProcedure(String hql, Map<String, Object> paramMap, Class<T> targetClass);	
	
	List<Object> findByStoredProcedure(String hql, Object[] params);
	
	<T> List<T> findByStoredProcedure(String hql, Object[] params, Class<T> targetClass);
	
	<T> T findSingleByNativeSql(String sql, Map<String, Object> paramMap, Class<T> targetClass);
	
	EntityManager getEntityManager();
}
