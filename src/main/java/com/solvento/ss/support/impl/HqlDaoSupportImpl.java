package com.solvento.ss.support.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.solvento.ss.support.BaseJpaCallback;
import com.solvento.ss.support.HqlDaoSupport;

@Repository
public class HqlDaoSupportImpl implements HqlDaoSupport {
	private static final Log logger = LogFactory.getLog(HqlDaoSupportImpl.class);

	@PersistenceContext
	private EntityManager entityManager;
	private EntityManagerFactory entityManagerFactory;

	@Override
	public void batchExecuteBySql(String hql) {
		try {
			Query query = entityManager.createQuery(hql);

			entityManager.getTransaction().begin();
			query.executeUpdate();
			entityManager.getTransaction().commit();
		} catch (RuntimeException e) {
			logger.error("batch execute by hql failed: " + e.getMessage(), e);
			throw e;
		}
	}

	@Override
	public int countAll(Class targetClass) {
		return countByHql("from " + targetClass.getSimpleName(), new Object[0]);
	}

	@Override
	public int countByHql(String hql, Map<String, Object> paramMap) {
		List<Object> list = findByHql("select sum(1) " + hql, paramMap);
		if (list.size() > 0) {
			Object obj = list.get(0);
			if (obj != null) {
				return new Long(obj.toString()).intValue();
			}
		}
		return 0;
	}

	@Override
	public int countByHql(String hql, Object[] params) {
		List<Object> list = findByHql("select sum(1) " + hql, params);
		if (list.size() > 0) {
			Object obj = list.get(0);
			if (obj != null) {
				return new Long(obj.toString()).intValue();
			}
		}
		return 0;
	}

	@Override
	public List<Object> findByHql(String hql, Map<String, Object> paramMap) {
		return findByHql(hql, paramMap, Object.class);
	}

	@Override
	public <T> List<T> findByHql(String hql, Map<String, Object> paramMap, Class<T> targetClass) {
		return findByHql(hql, paramMap, -1, -1, targetClass);
	}

	@Override
	public List<Object> findByHql(String hql, Map<String, Object> paramMap, int offset, int limit) {
		return findByHql(hql, paramMap, offset, limit, Object.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> findByHql(String hql, Map<String, Object> paramMap, int offset, int limit,
			Class<T> targetClass) {
		try {
			return (List<T>) new BaseJpaCallback(hql, paramMap, offset, limit)
					.doInJpa(entityManager);
		} catch (RuntimeException e) {
			logger.error("find by hql failed: " + e.getMessage(), e);
			throw e;
		}
	}

	@Override
	public List<Object> findByHql(String hql, Object[] params) {
		return findByHql(hql, params, -1, -1, Object.class);
	}

	@Override
	public <T> List<T> findByHql(String hql, Object[] params, Class<T> targetClass) {
		return findByHql(hql, params, -1, -1, targetClass);
	}

	@Override
	public List<Object> findByHql(String hql, Object[] params, int offset, int limit) {
		return findByHql(hql, params, offset, limit, Object.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> findByHql(String hql, Object[] params, int offset, int limit,
			Class<T> targetClass) {
		try {
			return (List<T>) new BaseJpaCallback(hql, params, offset, limit).doInJpa(entityManager);
		} catch (RuntimeException e) {
			logger.error("find by hql failed: " + e.getMessage(), e);
			throw e;
		}
	}

	@Override
	public <T> List<T> findByProperty(String propertyName, Object value, Class<T> targetClass) {
		String hql = "from " + targetClass.getSimpleName() + " where " + propertyName + " = ?";
		Object[] params = new Object[] { value };
		return findByHql(hql, params, targetClass);
	}

	@Override
	public List<Object> findByStoredProcedure(String hql, Map<String, Object> paramMap) {
		return findByStoredProcedure(hql, paramMap, Object.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> findByStoredProcedure(String hql, Map<String, Object> paramMap,
			Class<T> targetClass) {
		try {
			return (List<T>) new BaseJpaCallback(hql, paramMap, -1, -1)
					.doInJpaNativeQuery(entityManager);
		} catch (RuntimeException e) {
			logger.error("find by hql failed: " + e.getMessage(), e);
			throw e;
		}
	}

	@Override
	public List<Object> findByStoredProcedure(String hql, Object[] params) {
		return findByStoredProcedure(hql, params, Object.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> findByStoredProcedure(String hql, Object[] params, Class<T> targetClass) {
		try {
			return (List<T>) new BaseJpaCallback(hql, params, -1, -1)
					.doInJpaNativeQuery(entityManager);
		} catch (RuntimeException e) {
			logger.error("find by hql failed: " + e.getMessage(), e);
			throw e;
		}
	}

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}

	@Override
	public List<Object> findByNativeSql(String sql, int offset, int limit) {
		return findByNativeSql(sql, offset, limit, Object.class);
	}

	@Override
	public <T> List<T> findByNativeSql(String sql, int offset, int limit, Class<T> targetClass) {
		return entityManager.createNativeQuery(sql, targetClass).getResultList();
	}

	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public <T> T findSingleByNativeSql(String sql, Map<String, Object> paramMap,
			Class<T> targetClass) {
		Query query = entityManager.createNativeQuery(sql);

		for (String key : paramMap.keySet()) {
			query.setParameter(key, paramMap.get(key));
		}

		return (T) query.getSingleResult();
	}

}