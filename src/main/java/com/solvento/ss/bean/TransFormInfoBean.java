package com.solvento.ss.bean;

public class TransFormInfoBean {

	private Integer sfId;
	private String fileName;
	private Integer streamWidth;
	private Integer streamHeight;
	private Double streamLength;
	private String transcodeStatus;

	public TransFormInfoBean(String fileName, int width, int height, double length, String status) {
		this.sfId = Integer.valueOf(Integer.parseInt(fileName.substring(0, fileName.indexOf("."))));
		this.fileName = fileName;
		this.streamHeight = Integer.valueOf(height);
		this.streamWidth = Integer.valueOf(width);
		this.streamLength = Double.valueOf(length);
		this.transcodeStatus = status;
	}

	public TransFormInfoBean() {
	}

	public Integer getSfId() {
		return sfId;
	}

	public void setSfId(Integer sfId) {
		this.sfId = sfId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Integer getStreamWidth() {
		return streamWidth;
	}

	public void setStreamWidth(Integer streamWidth) {
		this.streamWidth = streamWidth;
	}

	public Integer getStreamHeight() {
		return streamHeight;
	}

	public void setStreamHeight(Integer streamHeight) {
		this.streamHeight = streamHeight;
	}

	public Double getStreamLength() {
		return streamLength;
	}

	public void setStreamLength(Double streamLength) {
		this.streamLength = streamLength;
	}

	public String getTranscodeStatus() {
		return transcodeStatus;
	}

	public void setTranscodeStatus(String transcodeStatus) {
		this.transcodeStatus = transcodeStatus;
	}

	public String toString() {
		return "TransFormInfoBean [sfId=" + this.sfId + ", fileName=" + this.fileName + ", streamWidth=" + this.streamWidth + ", streamHeight="
				+ this.streamHeight + ", streamLength=" + this.streamLength + ", transcodeStatus=" + this.transcodeStatus + "]";
	}
}
