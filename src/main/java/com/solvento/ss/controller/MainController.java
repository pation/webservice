package com.solvento.ss.controller;

import com.solvento.ss.bean.TransFormInfoBean;
import com.solvento.ss.service.ExceptionLogService;
import com.solvento.ss.service.TranscodeService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MainController {
	
	@Resource
	private TranscodeService transcodeService;
	@Resource
	private ExceptionLogService exceptionLogService;

	@RequestMapping(
			value = { "/sendTransFormInfo" },
			method = { org.springframework.web.bind.annotation.RequestMethod.GET },
			produces = { "text/html; charset=utf-8" }
	)
	@ResponseBody
	public String sendTransFormInfo(HttpServletRequest request) throws Exception {
		// f=檔名   w=寬度   h=高度   l=時間長度(秒) s=狀態，R：執行中、E：錯誤、F：完成

		String fileName = (request.getParameter("f") == null || request.getParameter("f").isEmpty()) ? null
				: request.getParameter("f");

		int width = (request.getParameter("w") == null || request.getParameter("w").isEmpty()) ? 0
				: Integer.parseInt(request.getParameter("w"));

		int height = (request.getParameter("h") == null || request.getParameter("h").isEmpty()) ? 0
				: Integer.parseInt(request.getParameter("h"));

		String status = (request.getParameter("s") == null || request.getParameter("s").isEmpty()) ? null
				: request.getParameter("s");

		double length = (request.getParameter("l") == null || request.getParameter("l").isEmpty()) ? 0
				: Double.parseDouble(request.getParameter("l"));

		if (fileName == null || status == null){
			return "ERROR: 資料不完全";
		}else {
			TransFormInfoBean TFBean = new TransFormInfoBean(fileName, width, height, length, status);
			try {
				return this.transcodeService.updateTranFromBySP(TFBean) ? "Update Success" : "Update Fail";
			} catch (Exception e) {
				exceptionLogService.insertNewLog("轉檔狀態修正",
						"f="+request.getParameter("f")
						+",w="+request.getParameter("w")
						+",h="+request.getParameter("h")
						+",s="+request.getParameter("s")
						+",l="+request.getParameter("l"),
						e.getMessage(), e.fillInStackTrace().getStackTrace());
				return "Update Error: " + e.getMessage();
			}
		}
		
	}
	
}
