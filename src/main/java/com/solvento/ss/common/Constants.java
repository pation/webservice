package com.solvento.ss.common;

public class Constants {

	public static final String CHARACTER_ENCODING = "UTF-8";
	public static final short SEARCH_LOG_STYLE_BASIC = 0;
	public static final short SEARCH_LOG_STYLE_ADV = 1;
	public static final short SEARCH_LOG_STYLE_DETAIL_CLICKING = 2;
	public static final String DATE_FORMAT_PATTERN_1 = "yyyyMMddHHmmssSSS";
	public static final String DATE_FORMAT_PATTERN_2 = "yyyy/MM/dd";
	public static final String DATE_FORMAT_PATTERN_3 = "yyyy/MM/dd HH:mm";
	public static final String DATE_FORMAT_PATTERN_4 = "MMdd";
	public static final String DATE_FORMAT_PATTERN_5 = "yyyyMMdd";
	public static final String DATE_FORMAT_PATTERN_6 = "yyyy/MM/dd a KK:mm:ss";
	public static final String DATE_FORMAT_PATTERN_7 = "yyyy-MM-dd HH:mm:ss";
	public static final String DATE_FORMAT_PATTERN_8 = "yyyy/MM/dd HH:mm:ss";
	public static final String CONTENT_TYPE_JSON = "application/json; charset=UTF-8";
	public static final String CONTENT_TYPE_PLAIN = "text/plain; charset=UTF-8";
	public static final String CONTENT_TYPE_HTML = "text/html; charset=UTF-8";
	public static final String CONTENT_TYPE_XML = "text/xml; charset=UTF-8";
	public static final String CONTENT_TYPE_JAVASCRIPT = "text/javascript; charset=UTF-8";
	public static final String CONTENT_TYPE_PNG = "image/png";

}
