package com.solvento.ss.service;

public interface ExceptionLogService {

	void insertNewLog(String className, String methodName, String exceptionMessage, StackTraceElement[] stes);
	
}
