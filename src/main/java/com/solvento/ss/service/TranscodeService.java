package com.solvento.ss.service;

import com.solvento.ss.bean.TransFormInfoBean;

public abstract interface TranscodeService {
	
	public abstract boolean updateTranFromBySP(TransFormInfoBean paramTransFormInfoBean) throws Exception;
	
}
