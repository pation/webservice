package com.solvento.ss.service.impl;

import com.solvento.ss.bean.TransFormInfoBean;
import com.solvento.ss.service.ExceptionLogService;
import com.solvento.ss.service.TranscodeService;
import com.solvento.ss.support.HqlDaoSupport;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class TranscodeServiceImpl implements TranscodeService {

	private static final Logger logger = LoggerFactory.getLogger(TranscodeServiceImpl.class);

	@Resource
	private HqlDaoSupport hqlDao;
	@Resource
	private ExceptionLogService exceptionLogService;

	public boolean updateTranFromBySP(TransFormInfoBean tfInfoBean) throws Exception {
		
		logger.info(new Date().toString() + "," + tfInfoBean.getFileName() + "," + tfInfoBean.getTranscodeStatus() + ","
				+ tfInfoBean.getStreamWidth() + "," + tfInfoBean.getStreamHeight() + "," + tfInfoBean.getStreamLength());

		String hql = "call sp_transcode_stream(:status , :sfId , :width , :height , :length)";

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("status", tfInfoBean.getTranscodeStatus());
		paramMap.put("sfId", tfInfoBean.getSfId());
		paramMap.put("width", tfInfoBean.getStreamWidth());
		paramMap.put("height", tfInfoBean.getStreamHeight());
		paramMap.put("length", tfInfoBean.getStreamLength());
		
		try {
			hqlDao.findByStoredProcedure(hql, paramMap);
			return true;
		} catch (Exception e) {
			logger.info(new Date().toString() + "," + tfInfoBean.getFileName() + ",E," + tfInfoBean.getStreamWidth() + ","
					+ tfInfoBean.getStreamHeight() + "," + tfInfoBean.getStreamLength());
			logger.error(e.getMessage());
			exceptionLogService.insertNewLog(
					"StreamFileServiceImpl",
					"(轉檔狀態修正) -> updateTranFromBySP",
					e.getMessage(),
					e.fillInStackTrace().getStackTrace());
		}
		return false;
	}
	
}
