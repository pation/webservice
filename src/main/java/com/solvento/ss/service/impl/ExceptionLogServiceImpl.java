package com.solvento.ss.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.solvento.ss.service.ExceptionLogService;
import com.solvento.ss.support.HqlDaoSupport;

@Service
public class ExceptionLogServiceImpl implements ExceptionLogService {

	@Resource
	private HqlDaoSupport hqlDao;

	private static final String INSERT_NEW_LOG = "call sp_insert_exception_log(:class,:method,:exception)";
	
	@Override
	public void insertNewLog(String className, String methodName, String exceptionMessage, StackTraceElement[] stes) {
		String str = exceptionMessage + "\n";
//		for(StackTraceElement ste: stes){
//			str += ste +"\n";
//		}
		str += stes[0] + "\n";
		if(stes.length > 1) str += stes[1] + "\n";
		if(stes.length > 2) str += stes[2] + "\n";
		if(stes.length > 3) str += stes[3] + "\n";
		if(stes.length > 4) str += stes[4] + "\n";
		if(stes.length > 5) str += stes[5] + "\n";
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("class", "後台 " + className);
		paramMap.put("method", methodName);
		paramMap.put("exception", str);
		hqlDao.findByStoredProcedure(INSERT_NEW_LOG, paramMap);
	}

}
